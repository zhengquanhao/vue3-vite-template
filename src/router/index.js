import { createRouter, createWebHistory } from "vue-router";

// 开启历史模式
// vue2中使用 mode: history 实现
const routerHistory = createWebHistory();

const router = createRouter({
    history: routerHistory,
    routes: [
        {
            path: "/",
            redirect: "/home"
        },
        {
            path: "/home",
            name: "home",
            component: () => import("@/views/Home.vue")
        },
        {
            path: "/about",
            name: "about",
            component: () => import("@/views/About.vue")
        },
        {
            path: "/test",
            name: "test",
            component: () => import("@/views/Test.vue")
        },
        // 在参数中最定义正则
        {
            path: "/article/:id(\\d+)",
            component: () => import("@/views/Article.vue")
        },
        // 可重复参数
        // ‘+’（至少一个参数） ’*‘（0到任意个）  ’？‘（0次或一次） 规则与正则一致
        {
            path: "/news/:id+",
            component: () => import("@/views/News.vue")
        },
        // 不在路由列表的路径返回错误页
        {
            path: "/:path(.*)",
            component: () => import("@/views/NotFound.vue")
        },
        // 嵌套路由
        {
            path: "/user",
            component: () => import("@/views/user/User.vue"),
            children: [
                {
                    path: "userInfo", // 前面不需要加’/‘
                    component: () => import("@/views/user/UserInfo.vue")
                },
                {
                    path: "userLogin", // 前面不需要加’/‘
                    component: () => import("@/views/user/userLogin.vue")
                }
            ]
        }
    ]
});

export default router;