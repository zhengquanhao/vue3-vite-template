import { createI18n } from "vue-i18n";
import cookie from "@/utils/cookie";
import * as id from "./in_ID";
import * as en from "./en_US";
import * as cn from "./zh_CN";
import ElementPlus from "element-plus";
import elementZh from "element-plus/es/locale/lang/zh-cn";
import elementEn from "element-plus/es/locale/lang/en";
import elementId from "element-plus/es/locale/lang/id";

const elementMap = {
    in_ID: elementId,
    en_US: elementEn,
    zh_CN: elementZh
};

const install = (app) => {
    const i18n = createI18n({
        locale: cookie.getCookie("language") || "in_ID",
        messages: {
            in_ID:  {
                ...id,
                ...elementId
            },
            en_US: {
                ...en,
                ...elementEn
            },
            zh_CN: {
                ...cn,
                ...elementZh
            }
        }
    });
    app.use(ElementPlus, {
        locale: elementMap[cookie.getCookie("language") || "in_ID"]
    });
    app.use(i18n);
};
export default install;