/*
 * 曝光上报
 */
const pushCaches = []; // 缓存上报数据
const eventListeners = new Set(); // 上报对象的检查事件队列
let clientWidth = window.innerWidth || document.documentElement.clientWidth;
let clientHeight = (window.innerHeight || document.documentElement.clientHeight) - 50;
let isTicking = false;
/**
 * 处理事件
 */
function pushEvent () {
    eventListeners.forEach((listener) => {
        listener();
    });
    isTicking = false;
}

function hanldePushEvent () {
    if (!isTicking) {
        if (window.requestAnimationFrame) {
            window.requestAnimationFrame(pushEvent);
        } else {
            pushEvent();
        }
        isTicking = true;
    }
}

// 计算宽高
window.addEventListener("resize", (e) => {
    clientWidth = window.innerWidth || document.documentElement.clientWidth;
    clientHeight = (window.innerHeight || document.documentElement.clientHeight) - 50;
    hanldePushEvent();
}, true);

window.addEventListener("scroll", hanldePushEvent, true);
// 特色情况下遗漏的监控
setInterval(hanldePushEvent, 1000);

/**
 * 计算元素是否可见
 */
function isInViewport (el) {
    const rect = el.getBoundingClientRect();
    const rectParent = el.parentNode.getBoundingClientRect();

    // 是否在窗口内
    const isInClient = ((rect.left >= 0 && rect.left < clientWidth) && (rect.top >= 0 && rect.top < clientHeight)) ||
        ((rect.right > 0 && rect.right <= clientWidth) && (rect.bottom > 0 && rect.bottom <= clientHeight));
    // 是否在父级内
    const isInParent = ((rect.left >= rectParent.left && rect.left < rectParent.right) && (rect.top >= rectParent.top && rect.top < rectParent.bottom)) ||
        ((rect.right > rectParent.left && rect.right <= rectParent.right) && (rect.bottom > rectParent.top && rect.bottom <= rectParent.bottom));

    return (
        isInClient && isInParent
    );
}
/**
 * 上报
 */
function emitPushLog () {
    if (window.expLogJSON) {
        if (pushCaches.length > 0) {
            // console.log('合并上报数据')
            // 合并上报数据
            const pushData = {};
            for (let i = 0, l = pushCaches.length; i < l; i++) {
                const onePush = pushCaches.pop();
                const {
                    pagename,
                    modulename,
                    ...other
                } = onePush;
                const key = Object.keys(other)[0];
                if (pushData[pagename]) {
                    if (pushData[pagename][modulename]) {
                        if (pushData[pagename][modulename][key]) {
                            pushData[pagename][modulename][key] += `,${other[key]}`;
                        } else {
                            pushData[pagename][modulename][key] = other[key];
                        }
                    } else {
                        pushData[pagename][modulename] = other;
                    }
                } else {
                    pushData[pagename] = {
                        [modulename]: other
                    };
                }
            }

            Object.keys(pushData).forEach((key) => {
                Object.keys(pushData[key]).forEach((key2) => {
                    Object.keys(pushData[key][key2]).forEach((key3) => {
                        const info = {
                            pagename: key,
                            modulename: key2,
                            [key3]: pushData[key][key2][key3]
                        };
                        // console.log(info);
                        window.expLogJSON("epi_m", "Home_exposure", JSON.stringify(info));
                    });
                });
            });
        }
    }
}
/**
 * 加入上报, 延时执行
 */
let st; // 延时计时器
function pushLog (info) {
    pushCaches.push(info);
    if (st) {
        clearTimeout(st);
    }
    st = setTimeout(emitPushLog, 400);
}
/**
 * 初始化上报设置
 */
function init (el, binding) {
    function listener () {
        if (isInViewport(el)) {
            pushLog(binding.value);
            eventListeners.delete(listener);
            el.removeEventListener("expLog", listener, false);
        }
    }
    eventListeners.add(listener);
    el.addEventListener("expLog", listener, false);
}

export default {
    inserted: init
};
