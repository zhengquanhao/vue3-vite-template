import expLog from "./explog";
import clsTag from "./clstag";

const vueDirective = {};
vueDirective.install = function install (Vue) {
    // 曝光
    Vue.directive("expLog", expLog);
    // 埋点
    Vue.directive("clstag", clsTag);
};

export default vueDirective;
