// 基础埋点前缀
const CLSTAG_BASE_PREFIX = "pageclick|keycount|";
export default {
    name: "clstag",
    inserted: function (el, binding) {
        const serviceValue = binding.value.value;
        const serviceKey = binding.value.key;
        if (!serviceValue) {
            return;
        }
        const clsTag = serviceKey ? (CLSTAG_BASE_PREFIX + serviceKey + "|" + serviceValue) : (CLSTAG_BASE_PREFIX + serviceValue);
        el.setAttribute("clstag", clsTag);
    }
};
