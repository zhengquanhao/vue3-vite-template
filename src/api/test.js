import fetch from "../common/fetch";

// const baseUrl = "https://test.com"; // mock
const baseUrl = process.env.NODE_ENV === "production" ? "接口域名" : "/seller"; // 代理

export function getServicePageList (params) {
    console.log("baseUrl", baseUrl);
    return fetch({
        url: baseUrl + "/seller/afterSales/getPage",
        method: "post",
        data: params
    }, "application/json");
}

// 获取是否开通自主售后功能
export function getIsOpenAfterSale () {
    return fetch({
        url: baseUrl + "/seller/afterSales/isOpenAfterSales",
        method: "get"
    });
}

// 获取商家原返信息
export function getReturnBack (params) {
    return fetch({
        url: baseUrl + "/seller/afterSales/getReturnBack",
        method: "post",
        data: params
    }, "application/json");
}

// get请求
export function getList (params) {
    return fetch({
        url: baseUrl + "/about/list",
        method: "get",
        params
    });
};

// post请求
export function addList (params) {
    return fetch({
        url: "接口地址",
        method: "post",
        data : params
    });
};

// post-application形式
// export function addList (params) {
//     return fetch({
//         url: "接口地址",
//         method: "post",
//         data : params
//     }, "application/json");
// };

// restful风格接口参数拼接
export function deleteList (id, params) {
    return fetch({
        url: `接口地址/${id}`,
        method: "delete",
        params
    });
};

// 上传-文件格式
export function upload (params) {
    return fetch({
        url: baseUrl + "/upload",
        method: "post",
        data: params
    }, "multipart/form-data");
}
