import * as apiTest from "./test";

const apiObj = {
    apiTest
};

const install = function (Vue) {
    if (install.installed) return;
    install.installed = true;

    Object.defineProperties(Vue.config.globalProperties, {
        $fetch: {
            get () {
                return apiObj;
            }
        }
    });
};

export default install;
