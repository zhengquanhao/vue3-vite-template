const NumFormat = num => {
    if (!num) return "0.00";

    if (num) {
        // eslint-disable-next-line no-useless-escape
        num = parseInt(num).toString().replace(/\$|\,/g, "");
        if (num === "" || isNaN(num)) {
            return "";
        }
        const sign = num.indexOf("-") > -1 ? "-" : "";
        if (num.indexOf("-") > -1) {
            num = num.substring(1);
        }
        let cents = num.indexOf(".") > 0 ? num.substr(num.indexOf(".")) : "";
        cents = cents.length > 1 ? cents : "";
        num = num.indexOf(".") > 0 ? num.substring(0, num.indexOf(".")) : num;
        if (cents === "") {
            if (num.length > 1 && num.substr(0, 1) === "0") {
                return "";
            }
        } else {
            if (num.length > 1 && num.substr(0, 1) === "0") {
                return "";
            }
        }
        for (let i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
            num = num.substring(0, num.length - (4 * i + 3)) + "," + num.substring(num.length - (4 * i + 3));
        }
        return sign + num + cents;
    } else {
        return "0";
    }
};

const dateFormat = (date, fmt) => {
    // 如果是时间戳的话那么转换成Date类型
    if (!date) {
        return;
    }
    // 如果不是时间戳，是字符串，如 2022-07-16
    if (isNaN(date)) {
        date = (new Date(date)).getTime();
    }
    const timezone = 7; // 目标时区时间，东七区
    const offsetGMT = new Date().getTimezoneOffset(); // 本地时间和格林威治的时间差，单位为分钟
    if (typeof date === "number") {
        date = new Date(date + offsetGMT * 60 * 1000 + timezone * 60 * 60 * 1000);
        // date = new Date(date);
    } else if (typeof date === "string") {
        // date = new Date(parseInt(date));
        date = new Date(parseInt(date + offsetGMT * 60 * 1000 + timezone * 60 * 60 * 1000));
    }
    const o = {
        // 月份
        "M+": date.getMonth() + 1,
        // 日
        "d+": date.getDate(),
        // 小时
        "h+": date.getHours(),
        // 分
        "m+": date.getMinutes(),
        // 秒
        "s+": date.getSeconds(),
        // 季度
        "q+": Math.floor((date.getMonth() + 3) / 3),
        // 毫秒
        S: date.getMilliseconds()
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (const k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
};

export { NumFormat, dateFormat };
