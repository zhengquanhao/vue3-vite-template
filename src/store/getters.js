export default {
    getLoadingState (state) {
        return state.isLoading;
    }
};