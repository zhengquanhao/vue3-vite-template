export default {
    setLoadingState (state, payload) {
        console.log(payload);
        if (payload) {
            state.loadingNum++;
            if (state.loadingNum > 0) {
                state.isLoading = true;
            }
        }
        if (!payload) {
            state.loadingNum--;
            if (state.loadingNum === 0) {
                state.isLoading = false;
            }
        }
    }
};