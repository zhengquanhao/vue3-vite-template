import { createApp } from "vue";
import api from "./api/index";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./i18n/index";
import * as filters from "./filters/index";
import direction from "./directives";
import VueLazyLoad from "vue3-lazyload";
import defaultImage from "./assets/images/default.jpeg";
// import "element-plus/dist/index.css";
import "@/style/normalize.css";

const app = createApp(App);

app.config.globalProperties.$filters = {
    ...filters
};

if (process.env.NODE_ENV === "development") {
    if ("__VUE_DEVTOOLS_GLOBAL_HOOK__" in window) {
    // 这里__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue赋值一个createApp实例
        window.__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue = app;
    }
    app.config.devtools = true;
}

app.use(api);
app.use(router);
app.use(store);
app.use(i18n);
app.use(direction);
app.use(VueLazyLoad, {
    loading: defaultImage, // 图片正在加载时显示的 loading 图片
    error: defaultImage // 图片加载异常时显示的 error 图片
});
app.mount("#app");