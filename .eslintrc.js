module.exports = {
    root: true,
    env: {
        browser: true,
        es6: true,
        "vue/setup-compiler-macros": true
    },
    parserOptions: {
        sourceType: "module"
    },
    extends: [
        "plugin:vue/vue3-recommended",
        "standard"
    ],
    plugins: [
        "vue"
    ],
    rules: {
        // 箭头函数规则需要加小括号
        "arrow-parens": 0,
        // 构造函数前后空格
        "generator-star-spacing": 0,
        // 禁止使用debug
        "no-debugger": 0,
        // 分号结尾
        semi: [2, "always"],
        // 强制双引号
        quotes: [2, "double"],
        // 变量、对象等前后空格
        "key-spacing": [0, { beforeColon: true, afterColon: true }],
        // 缩进
        indent: [2, 4],
        // 取消换行结束
        "eol-last": 0,
        "no-labels": ["error", { allowLoop: true }],
        "vue/multi-word-component-names": 0
    }
};