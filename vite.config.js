import { defineConfig } from "vite";
import { resolve } from "path";
import vue from "@vitejs/plugin-vue";
import autoprefixer from "autoprefixer";
import viteCompression from "vite-plugin-compression";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import importToCDN from "vite-plugin-cdn-import";

function pathResolve (dir) {
    return resolve(__dirname, ".", dir);
}

export default defineConfig({
    plugins:[
        vue(),
        viteCompression({
            threshold: 1024
        }),
        AutoImport({
            resolvers: [ElementPlusResolver()]
        }),
        Components({
            resolvers: [ElementPlusResolver()]
        }),
        importToCDN({
            modules: [
                // {
                //     name: "vue",
                //     var: "Vue",
                //     path: "//xxx.com/vue.min.js"
                // },
                // {
                //     name: "axios",
                //     var: "axios",
                //     path: "//xxx.com/common/axios.min.js"
                // }
            ]
        })
    ],
    resolve: {
        alias: {
            "@": pathResolve("src") // 这里是将src目录配置别名为 /@ 方便在项目中导入src目录下的文件
        }
    },
    css:{
        preprocessorOptions: {
            less: {
                javascriptEnabled: true
            }
        },
        postcss:{
            plugins:[
                autoprefixer()
            ]
        }
    },
    // 本地运行配置，及反向代理配置
    server: {
        host: "dev-seller.jd.id", // 默认是 localhost
        port: "8000", // 默认是 3000 端口
        cors: true, // 默认启用并允许任何源
        open: true, // 浏览器自动打开
        // 反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
        proxy: {
            "/api": {
                target: "http://目标地址", // 代理接口
                ws: false,
                secure: false,
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, "")
            },
            "/seller": {
                target: "http://seller-aftersales.jd.id",
                ws: true,
                secure: false,
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/seller/, "")
            }
        }
    },
    // 打包配置
    build: {
        // 指定输出路径
        outDir: "dist",
        // 生成静态资源的存放路径
        assetsDir: "assets",
        // 小于此阈值的导入或引用资源将内联为 base64 编码，以避免额外的 http 请求。设置为 0 可以完全禁用此项
        assetsInlineLimit: 4096,
        // 构建后是否生成 source map 文件
        sourcemap: true
    }
});